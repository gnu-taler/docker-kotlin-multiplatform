#!/usr/bin/env bash

SDK="http://android-rebuilds.beuc.net/dl/repository/sdk-repo-linux-platforms-eng.10.0.0_r14.zip"
SDK_TOOLS="http://android-rebuilds.beuc.net/dl/repository/sdk-repo-linux-tools-26.1.1.zip"
BUILD_TOOLS="http://android-rebuilds.beuc.net/dl/repository/sdk-repo-linux-build-tools-eng.10.0.0_r14.zip"
#BUILD_TOOLS_VERSION="29.0.2"

apt-get install -y --no-install-recommends \
	wget \
	unzip

# download archives
wget "$SDK" -O android-sdk.zip
wget "$SDK_TOOLS" -O android-sdk-tools.zip
wget "$BUILD_TOOLS" -O android-build-tools.zip

# ensure subfolders exist
mkdir -p "${ANDROID_SDK_ROOT}/platforms"
mkdir -p "${ANDROID_SDK_ROOT}/build-tools"

# extract archives
unzip android-sdk.zip -d "${ANDROID_SDK_ROOT}/platforms"
unzip android-sdk-tools.zip -d "${ANDROID_SDK_ROOT}"
unzip android-build-tools.zip -d "${ANDROID_SDK_ROOT}/build-tools"

# change location of build tools
#mv "${ANDROID_SDK_ROOT}/build-tools/android-10" "${ANDROID_SDK_ROOT}/build-tools/${BUILD_TOOLS_VERSION}"

# clean up archives
rm android-sdk.zip
rm android-sdk-tools.zip
rm android-build-tools.zip


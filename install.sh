#!/usr/bin/env bash
set -e
set -x

# update package sources
apt-get update
apt-get -y upgrade

# fix openjdk install bug
mkdir -p /usr/share/man/man1

# install packages
apt-get install -y --no-install-recommends \
	git \
	openjdk-11-jdk-headless \
	chromium \
	libsodium-dev \
	libcurl4-openssl-dev \
	libncurses5

./install-android-sdk.sh

# clean up for smaller image size
apt-get -y autoremove --purge
apt-get clean
rm -rf /var/lib/apt/lists/*

# allow headless chromium to run as root
echo 'export CHROMIUM_FLAGS="$CHROMIUM_FLAGS --no-sandbox"' >> /etc/chromium.d/default-flags

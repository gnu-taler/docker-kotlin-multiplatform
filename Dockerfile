FROM debian:buster-slim

ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive
ENV ANDROID_SDK_ROOT=/opt/android-sdk
ENV CHROME_BIN=/usr/bin/chromium

WORKDIR /opt/docker-kotlin-multiplatform

ADD install*.sh ./
ADD test.sh ./

RUN ./install.sh


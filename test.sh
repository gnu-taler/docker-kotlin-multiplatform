#!/usr/bin/env bash
set -e
set -x

git clone https://gitlab.com/gnu-taler/wallet-kotlin.git
cd wallet-kotlin
./gradlew check


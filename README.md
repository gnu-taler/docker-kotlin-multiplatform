Docker CI image to run [Kotlin Multiplatform](https://kotlinlang.org/docs/reference/multiplatform.html) tests.

It is based on Debian stable and includes Chromium for running headless browser tests.
Due to Chromium and its dependencies, the image is rather big.

# Using this Image

With docker:

    docker run registry.gitlab.com/gnu-taler/docker-kotlin-multiplatform:latest yourscript

In `.gitlab-ci.yml` files:

```yaml
image: "registry.gitlab.com/gnu-taler/docker-kotlin-multiplatform:latest"
```

